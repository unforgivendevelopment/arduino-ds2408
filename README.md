# Arduino DS2408 Library #

This is a library for the Maxim _(previously **Dallas**)_ **DS2408**, which is an 8-channel switch device, which
communicates with a host microcontroller via the **OneWire** _(also known as: **1-Wire**)_ bus.
